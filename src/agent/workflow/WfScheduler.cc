/**
 * @file WfScheduler.cc
 *
 * @brief  The MA DAG scheduler interface
 *
 * @author  Abdelkader AMAR (Abdelkader.Amar@ens-lyon.fr)
 *
 * @section Licence
 *   |LICENCE|
 */

#include "WfScheduler.hh"

using namespace madag;

/**
 * WfScheduler constructor
 */
WfScheduler::WfScheduler() {
} // end WfScheduler constructor

/**
 * WfScheduler destructor
 */
WfScheduler::~WfScheduler() {
} // end WfScheduler desctructor
